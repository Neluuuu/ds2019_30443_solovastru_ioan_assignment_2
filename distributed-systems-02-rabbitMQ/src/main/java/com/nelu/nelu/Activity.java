package com.nelu.nelu;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Activity implements Serializable {

    private String patientUsername;
    private String activity;
    private String startActivity;
    private String endActivity;

    public Activity(String patientUsername, String activity, String startActivity, String endActivity) {
        this.patientUsername = patientUsername;
        this.activity = activity;
        this.startActivity = startActivity;
        this.endActivity = endActivity;
    }

    public Activity() {
    }

    public String getPatientUsername() {
        return patientUsername;
    }

    public String getActivity() {
        return activity;
    }

    public String getStartActivity() {
        return startActivity;
    }

    public String getEndActivity() {
        return endActivity;
    }

    public void setPatientUsername(String patientUsername) {
        this.patientUsername = patientUsername;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setStartActivity(String startActivity) {
        this.startActivity = startActivity;
    }

    public void setEndActivity(String endActivity) {
        this.endActivity = endActivity;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "patientUsername='" + patientUsername + '\'' +
                ", activity='" + activity + '\'' +
                ", startActivity='" + startActivity + '\'' +
                ", endActivity='" + endActivity + '\'' +
                '}';
    }
}
