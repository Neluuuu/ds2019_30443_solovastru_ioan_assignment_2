package com.nelu.nelu;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.ScheduledAnnotationBeanPostProcessor;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class SendMessage {
    private final RabbitTemplate rabbitTemplate;
    private List<Activity> activities = getActivities();
    private int index;

    @Autowired
    private ScheduledAnnotationBeanPostProcessor postProcessor;

    public SendMessage(final RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(fixedDelay = 2000L)
    public void sendMessage() {
        if (index < activities.size()) {
            System.out.println(index);
            rabbitTemplate.convertAndSend(RabbitMQApplication.EXCHANGE_NAME, RabbitMQApplication.ROUTING_KEY, activities.get(index++));
        } else {
            return;
        }
    }

    private List<Activity> getActivities() {
        //add patients
        List<String> patients = new ArrayList<>();
        patients.add("cristi@gmail.com");
        patients.add("ioana@gmail.com");

        List<Activity> activities = new ArrayList<>();
        try {
            File file = new File("src/main/resources/activity.txt");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String activityLine;

            while ((activityLine = br.readLine()) != null) {
                String[] splitActivity = activityLine.split("\t\t");
                Random random = new Random();
                int index = random.nextInt(patients.size());

                //Create activity
                Activity activity = new Activity();
                activity.setPatientUsername(patients.get(index));
                activity.setActivity(splitActivity[2].split("[ \t]")[0]); //delete spaces or tabs
                activity.setStartActivity(splitActivity[0]);
                activity.setEndActivity(splitActivity[1]);
                activities.add(activity);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return activities;
    }
}
